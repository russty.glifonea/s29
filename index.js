//[Insert Many]
db.users.insertMany([
	{
	firstName: "Jane",
	lastName: "Doe",
	age: 21.0,
	contact: {
		phone: "092624873312",
		email: "jdoe@gmail.com"
	},
	courses: ["Phyton", "React", "PHP"],
	department: "HR"
	},
	{
	firstName: "Stephen Hawkins",
	lastName: "Magalona",
	age: 76.0,
	contact: {
		phone: "09262487332",
		email: "shawkinsM@gmail.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "HR"
	},
	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82.0,
	contact: {
		phone: "09262487332",
		email: "shawkinsM@gmail.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "HR"
	},
])

// 2. Find users with letter s in their first name or d in their last name.
// a. Use the $or operator.
// b. Show only the firstName and lastName fields and hide the _id field.
db.users.find({
    $or: [
            { firstName: { $regex: "s", $options: "$i" }},
            { lastName: { $regex: "d", $options: "$i" }},
          ]
     }, {firstName: 1, lastName: 1, _id: 0})

// 3. Find users who are from the HR department and their age is greater than or equal to 70.
// a. Use the $and operator
db.users.find({
    $and: [
       {
         age: {$gte:70}
       }
    ]
})

//4. Find users with the letter e in their first name and has an age of less than or equal to 30.
//a. Use the $and, $regex and $lte operators
db.users.find({
	$and: [
        {
	firstName: { 
		$regex: "e",
	},
	age: {$lte:30}
        }
	]
})